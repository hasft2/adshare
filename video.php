<?php include "header.php"; ?>
    <div class="app__body">
        <div class="dashboard dashboard-js">
            <nav class="nav-bar text-center">
                <div class="nav-bar__menu">
                    <div class="nav-bar__menu__item">
                        <a href="">Home</a>
                    </div>
                    <div class="nav-bar__menu__item">
                        <a href="" class="drop-down btn__pop__trigger" data-target="btn__pop__category">Categories <i class="icon flat-icon flaticon-arrows-down"></i></a>

                    <div class="btn__pop btn__pop__category btn__pop--default">
                        <a href="">Category 1</a>
                        <a href="">Category 1</a>
                        <a href="">Category 1</a>
                    </div>

                    </div>
                </div>
            </nav>
            <aside class="sidebar">
                <div class="sidebar__header">
                    <button class="btn btn--clear btn-hamburger btn-collapsed-sidebar" data-target="dashboard">
                        <i class="icon flat-icon flaticon-hamburger"></i>
                    </button>
                </div>
                <div class="sidebar__body">
                    <div class="sidebar__body__inner">
                        <div class="app__block app__block--no-top">
                            <div class="user__info">
                                <div class="app__container">
                                    <div class="user__img" style="background-image:url('http://placehold.it/130x130')">
                                    </div>
                                    <div class="user__meta text-center">
                                        <p class="user__name">User Name</p>
                                        <p class="user__total__value text-orange">
                                            <span class="score">3.250</span>
                                            <span>Points</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="user__buttons">
                                <div class="app__container">
                                    <a class="btn btn--style-one btn--style-one--grey btn--style-dashboard" href="">Redeem</a>
                                    <a class="btn btn--style-one btn--style-one--grey btn--style-dashboard" href="">See Full Profile</a>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-style-one">
                        <div class="user__misc">
                            <div class="user__misc__inner">
                                <div class="app__list">
                                    <a href="">
                                        <i class="icon flat-icon flaticon-play"></i>
                                        <span class="text-inline">Running Ads</span>
                                    </a>
                                    <a href="">
                                        <i class="icon flat-icon flaticon-clock"></i>
                                        <span class="text-inline">History</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-style-one">
                        <div class="user__redeem">
                            <div class="user__redeem__inner">
                                <div class="app__list">
                                    <a href="">
                                        <i class="icon flat-icon flaticon-money-1"></i>
                                        <span class="text-inline">Redeem Prize</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-style-one">
                        <div class="user__subscription">
                            <div class="user__subscription__inner">
                                <div class="app__list">
                                    <a href="">
                                        <i class="icon flat-icon flaticon-stamp"></i>
                                        <span class="text-inline">Brand Subscription</span>
                                    </a>

                                    <ul class="brand__profile-subscribe">
                                        <li>
                                            <a href="" class="flex-container">
                                                <span class="brand__profile-img" style="background-image: url('http://placehold.it/22x22')"></span>
                                                <span class="brand__profile-name">Buavita Buavita Buavita Buavita</span>
                                            </a>
                                        </li>
                                    
                                        <li>
                                            <a href="" class="flex-container">
                                                <span class="brand__profile-img" style="background-image: url('http://placehold.it/22x22')"></span>
                                                <span class="brand__profile-name">Buavita</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar__footer">
                        <div class="follow flex-container">
                            <a href="">
                                <i class="icon flat-icon flaticon-facebook"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-youtube"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-twitter"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-instagram"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-social-media"></i>
                            </a>
                        </div>
                        <div class="dashboard-copyright text-center">
                            Copyright 2016 adshare.id
                        </div>
                    </div>
                </div>
            
            </aside>
            <div class="dashboard__main" data-dashboard="video">
                <div class="dashboard__main__inner">
                    <div class="card">
                        <div class="card__header">
                            <form action="" class="form-style-one">   
                            <div class="video__wrapper videoplyr">
                                <div class="question-step">
                                    <h1>First</h1>
                                    <div>
                                         <div class="step" data-step="1">
                                            <div class="step__header">
                                                <div class="step__header__inner flex-container card__container card__container--m-v">
                                                    <h2 class="step__number text-orange">Step 1</h2>
                                                    <div class="inline flex-container">
                                                        <span class=""><em class="text-orange">15</em> Point Rewards</span>
                                                        <span class=""><em class="text-orange">15</em> Question</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="step__body text-center card__container">
                                                <div class="question-step__text">
                                                    <p>Apakah anda suka dgn iklan ini?</p>
                                                    <div class="check-custom__yes-no flex-container">
                                                        <div class="check-custom">
                                                            <div class="check-custom__inner">
                                                                <input type="radio" id="nokip" name="q1">
                                                                <label for="nokip">Yes</label>
                                                            </div>
                                                        </div>
                                                        <div class="check-custom">
                                                            <div class="check-custom__inner">
                                                                <input type="radio" id="gosip" name="q1">
                                                                <label for="gosip">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn--style-one btn--style-dashboard-orange btn-next">Berikutnya</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h1>second</h1>
                                    <div>
                                         <div class="step" data-step="2">
                                            <div class="step__header">
                                                <div class="step__header__inner flex-container card__container card__container--m-v">
                                                    <h2 class="step__number text-orange">Step 2</h2>
                                                    <div class="inline flex-container">
                                                        <span class=""><em class="text-orange">15</em> IDR</span>
                                                        <span class=""><em class="text-orange">15</em> Question</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="step__body text-center card__container">
                                                <div class="question-step__text">
                                                    <p>Apakah anda mengenal produk ini?</p>
                                                    <div class="check-custom__yes-no flex-container">
                                                        <div class="check-custom">
                                                            <div class="check-custom__inner">
                                                                <input type="radio" id="yes" name="q2">
                                                                <label for="yes">Yes</label>
                                                            </div>
                                                        </div>
                                                        <div class="check-custom">
                                                            <div class="check-custom__inner">
                                                                <input type="radio" id="no" name="q2">
                                                                <label for="no">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn--style-one btn--style-dashboard-orange btn-next">Berikutnya</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h1>third</h1>
                                    <div>
                                         <div class="step" data-step="2">
                                            <div class="step__header">
                                                <div class="step__header__inner flex-container card__container card__container--m-v">
                                                    <h2 class="step__number text-orange">Step 3</h2>
                                                    <div class="inline flex-container">
                                                        <span class=""><em class="text-orange">15</em> IDR</span>
                                                        <span class=""><em class="text-orange">15</em> Question</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="step__body text-center card__container">
                                                <div class="question-step__text">
                                                    <p>Bagikan Minimal Ke 10 Teman Anda untuk mendapatkan <span class="text-orange">10 IDR</span></p>
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u=example.org" class="btn btn--style-one btn-share btn-next"> <i class="icon flat-icon flaticon-facebook"></i> Share Facebook</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h1>Fourth</h1>
                                    <div>
                                         <div class="step" data-step="3">
                                            <div class="step__header">
                                                <div class="step__header__inner flex-container card__container card__container--m-v">
                                                    <h2 class="step__number text-orange">03</h2>
                                                    <div class="inline flex-container">
                                                        <span class=""><em class="text-orange">15</em> IDR</span>
                                                        <span class=""><em class="text-orange">15</em> Question</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="step__body text-center card__container">
                                                <h3>Terima Kasih</h3>
                                                <p>Anda Mendapatkan</p>
                                                <div class="point-reward-box">
                                                    <h2>
                                                        <i class="icon flat-icon flaticon-money-1"></i>
                                                        <span class="text-orange">15</span>
                                                        IDR
                                                    </h2>
                                                </div>
                                                <p class="step__desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor i
                                                ncididunt ut labore et dolore magna aliqua.</p>
                                            </div>
                                            <div class="step__footer">
                                                <div class="card__container clearfix flex-container step__footer__inner">
                                                    <div class="step__thumbs clearfix show-medium">
                                                        <div class="step__thumb">
                                                            <img src="http://placehold.it/200x100" alt="">
                                                            <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                                <i class="icon flat-icon flaticon-play"></i>
                                                            </a>
                                                        </div>
                                                    
                                                        <div class="step__thumb">
                                                            <img src="http://placehold.it/200x100" alt="">
                                                            <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                                <i class="icon flat-icon flaticon-play"></i>
                                                            </a>
                                                        </div>
                                                    
                                                        <div class="step__thumb">
                                                            <img src="http://placehold.it/200x100" alt="">
                                                            <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                                <i class="icon flat-icon flaticon-play"></i>
                                                            </a>
                                                        </div>
                                                    
                                                        <div class="step__thumb">
                                                            <img src="http://placehold.it/200x100" alt="">
                                                            <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                                <i class="icon flat-icon flaticon-play"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="text-right">
                                                        <button class="btn btn--style-one btn--style-dashboard-orange btn-close" data-target="video__wrapper">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="js-media-player plyr">
                                    <video poster="http://placehold.it/700x395" controls>
                                        <!-- Video files -->
                                        <source src="video/big_buck_bunny.mp4" type="video/mp4">
                                        <source src="video/big_buck_bunny.webm" type="video/webm">

                                        <!-- Fallback for browsers that don't support the <video> element -->
                                        <a href="video/big_buck_bunny.mp4">Download</a>
                                    </video>       
                                </div>
                            </div>
                        </form>
                        </div>
                        <div class="card__sub-header" style="background-image:url()">
                            <div class="card__container">
                                <div class="card__sub-header__inner advertiser__inner">
                                    <div class="advertiser__info">
                                        <div class="advertiser__info__title flex-container">
                                            <h4>Buavita 2Go Rasa Apel</h4>
                                            <span class="point-label">
                                                <i class="icon flat-icon flaticon-money-1"></i>
                                                15 IDR
                                            </span>
                                        </div>
                                        <div class="advertiser__address flex-container">
                                            <div class="user__img user__img--small" style="background-image:url('http://placehold.it/45x45')"></div>
                                            <span>by Buavita.co.id</span>
                                        </div>
                                    </div>
                                    <div class="advertiser__misc">
                                        <h3 class="text-orange">13.876</h3>
                                        <button class="btn btn--style-one btn--style-dashboard-orange">
                                            <i class="icon ion-android-done"></i>
                                            <span class="spacer spacer--small"></span>
                                             Subscribe Brand
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-style-one hr-style-one--darker">
                        <div class="card__main">
                            <div class="card__container">
                                <div class="block">
                                    <div class="block__title">
                                        <h5>Waiting Ads To Watch</h5>
                                    </div>
                                    <div class="thumbs thumbs-style-one cols flex-container" data-flex-cols="3">
                                        <div class="thumb thumb--has-rupiah col">
                                            <div class="thumb__link" href="">
                                                <div class="thumb__img" style="background-image:url('http://placehold.it/250x130')">
                                                    <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                        <i class="icon flat-icon flaticon-play"></i>
                                                    </a>
                                                    <span class="duration">00:30</span>
                                                </div>
                                                <div class="thumb__info">
                                                    <a href="" class="thumb__title flex-container">
                                                        <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                                                    </a>
                                                    <div class="thumb__info__footer">
                                                        <span class="thumb__info__brand">by Buavita.co.id</span>
                                                        <span class="thumb__info__total-played text-orange">0 played</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="thumb thumb--has-rupiah col">
                                            <div class="thumb__link" href="">
                                                <div class="thumb__img" style="background-image:url('http://placehold.it/250x130')">
                                                    <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                        <i class="icon flat-icon flaticon-play"></i>
                                                    </a>
                                                    <span class="duration">00:30</span>
                                                </div>
                                                <div class="thumb__info">
                                                    <a href="" class="thumb__title flex-container">
                                                        <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                                                    </a>
                                                    <div class="thumb__info__footer">
                                                        <span class="thumb__info__brand">by Buavita.co.id</span>
                                                        <span class="thumb__info__total-played text-orange">0 played</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="thumb thumb--has-rupiah col">
                                            <div class="thumb__link" href="">
                                                <div class="thumb__img" style="background-image:url('http://placehold.it/250x130')">
                                                    <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                        <i class="icon flat-icon flaticon-play"></i>
                                                    </a>
                                                    <span class="duration">00:30</span>
                                                </div>
                                                <div class="thumb__info">
                                                    <a href="" class="thumb__title flex-container">
                                                        <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                                                    </a>
                                                    <div class="thumb__info__footer">
                                                        <span class="thumb__info__brand">by Buavita.co.id</span>
                                                        <span class="thumb__info__total-played text-orange">0 played</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include "footer.php"; ?>
