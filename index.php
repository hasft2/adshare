<?php include "header.php"; ?>
            <aside class="sidebar">
                <div class="sidebar__header">
                    <button class="btn btn--clear btn-hamburger btn-collapsed-sidebar" data-target="dashboard">
                        <i class="icon flat-icon flaticon-hamburger"></i>
                    </button>
                </div>
                <div class="sidebar__body">
                    <div class="sidebar__body__inner">
                        <div class="app__block app__block--no-top">
                            <div class="user__info">
                                <div class="app__container">
                                    <div class="user__img" style="background-image:url('http://placehold.it/130x130')">
                                    </div>
                                    <div class="user__meta text-center">
                                        <p class="user__name">User Name</p>
                                        <p class="user__total__value text-orange">
                                            <span class="score">3.250</span>
                                            <span>Points</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="user__buttons">
                                <div class="app__container">
                                    <a class="btn btn--style-one btn--style-one--grey btn--style-dashboard" href="">Withdraw</a>
                                    <a class="btn btn--style-one btn--style-one--grey btn--style-dashboard" href="">See Full Profile</a>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-style-one">
                        <div class="user__misc">
                            <div class="user__misc__inner">
                                <div class="app__list">
                                    <a href="">
                                        <i class="icon flat-icon flaticon-play"></i>
                                        <span class="text-inline">Running Ads</span>
                                    </a>
                                    <a href="">
                                        <i class="icon flat-icon flaticon-clock"></i>
                                        <span class="text-inline">History</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-style-one">
                        <div class="user__redeem">
                            <div class="user__redeem__inner">
                                <div class="app__list">
                                    <a href="">
                                        <i class="icon flat-icon flaticon-money-1"></i>
                                        <span class="text-inline">Redeem Prize</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-style-one">
                        <div class="user__subscription">
                            <div class="user__subscription__inner">
                                <div class="app__list">
                                    <a href="">
                                        <i class="icon flat-icon flaticon-stamp"></i>
                                        <span class="text-inline">Brand Subscription</span>
                                    </a>
                                    <ul class="brand__profile-subscribe">
                                        <li>
                                            <a href="" class="flex-container">
                                                <span class="brand__profile-img" style="background-image: url('http://placehold.it/22x22')"></span>
                                                <span class="brand__profile-name">Buavita Buavita Buavita Buavita</span>
                                            </a>
                                        </li>
                                    
                                        <li>
                                            <a href="" class="flex-container">
                                                <span class="brand__profile-img" style="background-image: url('http://placehold.it/22x22')"></span>
                                                <span class="brand__profile-name">Buavita</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar__footer">
                        <div class="follow flex-container">
                            <a href="">
                                <i class="icon flat-icon flaticon-facebook"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-youtube"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-twitter"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-instagram"></i>
                            </a>
                            <a href="">
                                <i class="icon flat-icon flaticon-social-media"></i>
                            </a>
                        </div>
                        <div class="dashboard-copyright text-center">
                            Copyright 2016 adshare.id
                        </div>
                    </div>
                </div>
            
            </aside>
            <div class="dashboard__main" data-dashboard="advertiser">
                <div class="dashboard__main__inner">
                    <div class="card">
                        <div class="card__main">
                            <div class="card__container card__container--m-v">
                                <div class="block">
                                        <div class="advertiser__ads-score">
                                            <div class="advertiser__ads-score__inner flex-container">
                                                <div class="advertiser__ads-score__item">
                                                    <span class="advertiser__ads-score__item__title">Positive</span>
                                                    <div class="advertiser__ads-score__item__points score-positive flex-container">
                                                        <i class="icon icon--w-border flat-icon flaticon-arrows-up"></i>
                                                        <h3 class="score">752</h3>
                                                    </div>
                                                </div>
                                                <div class="advertiser__ads-score__item">
                                                    <span class="advertiser__ads-score__item__title">Negative</span>
                                                    <div class="advertiser__ads-score__item__points score-negative flex-container">
                                                        <i class="icon icon--w-border flat-icon flaticon-arrows-down"></i>
                                                        <h3 class="score">752</h3>
                                                    </div>
                                                </div>
                                                <div class="advertiser__ads-score__item">
                                                    <span class="advertiser__ads-score__item__title">Total Engagement</span>
                                                    <div class="advertiser__ads-score__item__points score-current flex-container">
                                                        <i class="icon icon--w-border flat-icon flaticon-eye"></i>
                                                        <h3 class="score">752</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="card__container card__container--m-v channel-view-chart__container">
                                <div class="block">
                                    <div class="block__title flex-container">
                                        <span>Channel View</span>
                                        <nav class="nav-chart">
                                            <button id="reset" class="btn btn--style-one btn--style-one--block-grey btn--style-one--small active">reset</button>
                                            <button id="update" class="btn btn--style-one btn--style-one--block-grey btn--style-one--small">update</button>
                                        </nav>
                                    </div>
                                    <div class="chart">
                                        <div id="channel-view-chart"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="card__container card__container--m-v advertiser-running-ads">
                                <div class="block">
                                    <div class="block__title">
                                        <h5>Your Running Ads</h5>
                                    </div>

                                    <div class="thumbs thumbs-style-one thumbs--has-addnew cols flex-container" data-flex-cols="3">
                                        <div class="thumb thumb--addnew col">
                                            <div class="col__inner">
                                                <button class="btn btn__add-new-ads md-trigger" data-modal="modal-add-video">
                                                        <i class="icon flat-icon flaticon-add"></i>
                                                        <span>Post new adds</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                            <hr class="hr-style-one">
                            <div class="card__container card__container--m-v">
                                <div class="block">
                                    <div class="block__title">
                                        <h5>Ads History</h5>
                                    </div>

                                    <div class="thumbs thumbs-style-one cols flex-container" data-flex-cols="3">
                                        <div class="thumb thumb--has-rupiah col">
                                            <div class="thumb__link" href="">
                                                <div class="thumb__img" style="background-image:url('http://placehold.it/250x130')">
                                                    <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                        <i class="icon flat-icon flaticon-play"></i>
                                                    </a>
                                                    <span class="duration">00:30</span>
                                                </div>
                                                <div class="thumb__info">
                                                    <a href="" class="thumb__title flex-container">
                                                        <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                                                        <i class="icon icon-rupiah"></i>
                                                    </a>
                                                    <div class="thumb__info__footer">
                                                        <span class="thumb__info__brand">by Buavita.co.id</span>
                                                        <span class="thumb__info__total-played text-orange">0 played</span>
                                                    </div>
                                                    <div class="thumb__info__utils flex-container">
                                                        <div class="thumb__info__util">
                                                            Affiliate: <span class="text-orange">2k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Public: <span class="text-orange">10k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Reach: <span class="text-orange">2k</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="thumb thumb--has-rupiah col">
                                            <div class="thumb__link" href="">
                                                <div class="thumb__img" style="background-image:url('http://placehold.it/250x130')">
                                                    <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                        <i class="icon flat-icon flaticon-play"></i>
                                                    </a>
                                                    <span class="duration">00:30</span>
                                                </div>
                                                <div class="thumb__info">
                                                    <a href="" class="thumb__title flex-container">
                                                        <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                                                        <i class="icon icon-rupiah"></i>
                                                    </a>
                                                    <div class="thumb__info__footer">
                                                        <span class="thumb__info__brand">by Buavita.co.id</span>
                                                        <span class="thumb__info__total-played text-orange">0 played</span>
                                                    </div>
                                                    <div class="thumb__info__utils flex-container">
                                                        <div class="thumb__info__util">
                                                            Affiliate: <span class="text-orange">2k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Public: <span class="text-orange">10k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Reach: <span class="text-orange">2k</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="thumb thumb--has-rupiah col">
                                            <div class="thumb__link" href="">
                                                <div class="thumb__img" style="background-image:url('http://placehold.it/250x130')">
                                                    <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                        <i class="icon flat-icon flaticon-play"></i>
                                                    </a>
                                                    <span class="duration">00:30</span>
                                                </div>
                                                <div class="thumb__info">
                                                    <a href="" class="thumb__title flex-container">
                                                        <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                                                        <i class="icon ion-social-usd icon-rupiah"></i>
                                                    </a>
                                                    <div class="thumb__info__footer">
                                                        <span class="thumb__info__brand">by Buavita.co.id</span>
                                                        <span class="thumb__info__total-played text-orange">0 played</span>
                                                    </div>
                                                    <div class="thumb__info__utils flex-container">
                                                        <div class="thumb__info__util">
                                                            Affiliate: <span class="text-orange">2k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Public: <span class="text-orange">10k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Reach: <span class="text-orange">2k</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="thumb thumb--has-rupiah col">
                                            <div class="thumb__link" href="">
                                                <div class="thumb__img" style="background-image:url('http://placehold.it/250x130')">
                                                    <a href="" class="btn btn--icon btn--style-one btn--style-one--circle-outline btn--style-dashboard btn-play">
                                                        <i class="icon flat-icon flaticon-play"></i>
                                                    </a>
                                                    <span class="duration">00:30</span>
                                                </div>
                                                <div class="thumb__info">
                                                    <a href="" class="thumb__title flex-container">
                                                        <span>Thumb title maskdmsakd dmaskdmaskd daskdmaskdmsad dsamkdmas</span>
                                                        <i class="icon ion-social-usd icon-rupiah"></i>
                                                    </a>
                                                    <div class="thumb__info__footer">
                                                        <span class="thumb__info__brand">by Buavita.co.id</span>
                                                        <span class="thumb__info__total-played text-orange">0 played</span>
                                                    </div>
                                                    <div class="thumb__info__utils flex-container">
                                                        <div class="thumb__info__util">
                                                            Affiliate: <span class="text-orange">2k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Public: <span class="text-orange">10k</span>
                                                        </div>
                                                        <div class="thumb__info__util">
                                                            Reach: <span class="text-orange">2k</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include "footer.php"; ?>
