<!doctype html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ES6 Babel Browserify Boilerplate</title>

    <meta name="mobile-web-app-capable" content="yes">

    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,200,600,700|Roboto:400,100,300,500,700,400italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-minimal.css">
    <link rel="stylesheet" href="./css/style.min.css">
  </head>
  <body>

    <header class="app__header">
        <div class="app__container">
            <div class="app__header__inner flex-container">
                <div class="app__header__block-left clearfix flex-container">
                    <div class="clearfix flex-container">
                        <button class="btn btn--clear btn-menu__mobile btn-hamburger hide-medium btn-menu__mobile" data-target="dashboard">
                            <i class="icon flat-icon flaticon-hamburger"></i>
                        </button>
                        <a href="./" class="logo">
                            <img src="img/dashboard-logo.png" alt="Ad Share" class="logo-img">
                            <span class="logo-caption logo-caption--dashboard">Viral Ads Start Now</span>
                        </a>
                    </div>
                    <form action="" class="form-style-one form-style-one--lighter form-search">
                        <label for="" class="sr-only">search</label>
                        <input type="text">
                        <i class="icon flat-icon flaticon-magnify"></i>
                    </form>
                </div>
                <div class="app__header__util show-medium">
                    <span class="app__header__util__item">
                        <button class="btn btn-notif btn__pop__trigger" data-target="btn__pop__notif" data-notif="true">
                            <i class="icon flat-icon flaticon-bell"></i>
                            <span class="total-notif">10</span>
                        </button>
                        <div class="btn__pop btn__pop__notif btn__pop--default">
                                <i class="icon icon-arrow ion-arrow-up-b"></i>
                                <div class="btn__pop__header flex-container">
                                    <span><b>Notifikasi</b></span>
                                    <a href="">See All</a>
                                </div>
                                <a href="">Category 1</a>
                                <a href="">Category 1</a>
                                <a href="">Category 1</a>
                        </div>
                    </span>
                    <span class="app__header__util__item">
                        <button class="btn btn-menu__desktop btn__pop__trigger" data-target="btn__pop__desktop__menu">
                            <i class="icon flat-icon flaticon-arrows-down"></i>
                        </button>
                        <div class="btn__pop btn__pop__desktop__menu btn__pop--default">
                                <i class="icon icon-arrow ion-arrow-up-b"></i>
                                <div class="btn__pop__header flex-container">
                                    <span>Menu</span>
                                </div>
                                <a href="">Category 1</a>
                                <a href="">Category 1</a>
                                <a href="">Category 1</a>
                        </div>
                    </span>
                </div>
            </div>
        </div>
    </header>
